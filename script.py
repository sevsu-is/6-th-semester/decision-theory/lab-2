from typing import List


def dominations(A: List[List[int]], x: int) -> (List[int], List[int]):
    xPlus: List[int] = []
    xMinus: List[int] = []
    
    for i in range(x):
        if A[x][i] == 1:
            xMinus.append(i)
        if A[i][x] == 1:
            xPlus.append(i)

    return xPlus, xMinus


def getMin(xPlus: List[int], U: List[float]) -> int:
    minU: int = 0

    for x in xPlus:
        if U[x] < U[minU]:
            minU = x
    return minU
 

def getMax(xMinus: List[int], U: List[float]) -> int:
    maxU: int = 0

    for x in xMinus:
        if U[x] > U[maxU]:
            maxU = x
    return maxU        


def print_U(U: List[float]) -> None:
    print('')
    print('X', end='\t')
    for i in range(len(U)):
        print('x' + str(i + 1), end='\t')
    print('')

    print('U(X)', end='\t')
    for u in U:
        print(u, end='\t')
    print('', end='\n\n')
        

def main() -> None:
    A: List[List[int]] = [
        [1, 0, 0, 1, 0],
        [0, 1, 1, 0, 0],
        [0, 0, 1, 0, 0],
        [1, 0, 0, 1, 0],
        [0, 1, 0, 1, 1]
    ]

    U: List[float] = [0]

    for i in range(1, len(A)):
        xPlus, xMinus = dominations(A, i)
       
        emptySets: bool = xPlus == xMinus == []
        onlyDominates: bool = xPlus != [] and xMinus == []
        isOnlyDominated: bool = xPlus == [] and xMinus != []

        if emptySets:
            U.append(0)
        elif onlyDominates:
            minX = getMin(xPlus, U)
            U.append(minX - 1)
        elif isOnlyDominated:
            maxX = getMax(xMinus, U)
            U.append(maxX + 1)
        else:
            intersection: set = set(xPlus).intersection(xMinus)
            
            if len(intersection) != 0:
                U.append(U[intersection.pop()])
            else:
                U.append(getMin(xPlus, U) + getMax(xMinus, U) / 2)
    
    print_U(U)

                        
if __name__ == '__main__':
    main()

